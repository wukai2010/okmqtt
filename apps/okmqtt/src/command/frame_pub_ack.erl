%%%-------------------------------------------------------------------
%%% @author wukai
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 07. 十一月 2017 11:58
%%%QOS1消息确认收到
%%%-------------------------------------------------------------------
-module(frame_pub_ack).
-author("wukai").
-include("../include/mqtt_protocol.hrl").
%% API
-export([do_command/3]).
do_command(_, State, Frame) ->
  <<MSGID:16>> = Frame#mqtt_frame.left_bytes,
  lager:log(info, self(), "receive publish_ack ~p msgid", [MSGID]),
  erase(MSGID),%% 檫出进程字典里面id所对应的数据
  PacketIds = State#state.packetids,
  %%把此packageid放入未使用列表中
  State#state{packetids =PacketIds++[MSGID]}.
