%%%-------------------------------------------------------------------
%%% @author wukai
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 07. 十一月 2017 11:44
%%%-------------------------------------------------------------------
-module(frame_ping).
-author("wukai").
-include("../include/mqtt_protocol.hrl").

%% API
-export([do_command/3]).
do_command(_, State, _) ->
  log:log("receive ping from ~p",[State#state.client]),
  mqtt_process:send_binary(State, <<?FRAME_PING_ACK:4, 0:4, 0:8>>), State.


