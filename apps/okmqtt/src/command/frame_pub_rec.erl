%%%-------------------------------------------------------------------
%%% @author wukai
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 07. 十一月 2017 21:11
%%%-------------------------------------------------------------------
-module(frame_pub_rec).
-author("wukai").
-include("../include/mqtt_protocol.hrl").

%% API QOS交付第二步
-export([do_command/3]).
do_command(Ref, State, Frame) ->
  <<Mid:16>> = Frame#mqtt_frame.left_bytes,
  lager:log(info, self(), "pubrec cmd msgId:~p", [Mid]),
  pub_rel(Ref,State,Mid),
  State.

pub_rel(_, State, Mid) ->
  mqtt_process:send_binary(State,<<?FRAME_PUB_REL:4,0:4,2:8,Mid:16>>).