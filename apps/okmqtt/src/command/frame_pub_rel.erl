%%%-------------------------------------------------------------------
%%% @author wukai
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 07. 十一月 2017 21:11
%%%-------------------------------------------------------------------
-module(frame_pub_rel).
-author("wukai").
-include("../include/mqtt_protocol.hrl").
%% API
-export([do_command/3]).
do_command(Ref, State, Frame) ->
  <<Mid:16>> = Frame#mqtt_frame.left_bytes,
  lager:log(info, self(), "pubrel cmd msgId:~p", [Mid]),
  pub_comp(Ref,State,Mid),
  State.

pub_comp(_, State, Mid) ->
  mqtt_process:send_binary(State,<<?FRAME_PUB_COMP:4,0:4,2:8,Mid:16>>).