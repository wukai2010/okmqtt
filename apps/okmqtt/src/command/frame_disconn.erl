%%%-------------------------------------------------------------------
%%% @author wukai
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 08. 十一月 2017 11:48
%%%-------------------------------------------------------------------
-module(frame_disconn).
-author("wukai").
-include("../include/mqtt_protocol.hrl").

%% API
-export([do_command/3]).

do_command(Ref, State, Frame) when Frame#mqtt_frame.frame_flag == 0 ->
  if
    State#state.clean_session == 1 ->
      session_server:send_offline(State#state.client, State#state.clean_session);
    true -> ok end,
  ranch:remove_connection(Ref),
  mqtt_process:exit_self();

do_command(_, _, _) ->
  lager:log(info, self(), "disconnect frame_flag error"),
  mqtt_process:exit_self().

