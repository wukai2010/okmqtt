%%%-------------------------------------------------------------------
%%% @author wukai
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 08. 十一月 2017 09:23
%%%-------------------------------------------------------------------
-module(command).
-author("wukai").
%% API
-export([do_command/4]).
-include("../include/mqtt_protocol.hrl").
%%客户端连接
do_command(?FRAME_CONN, Ref, State, Frame) ->
  frame_conn:do_command(Ref, State, Frame);

%%心跳
do_command(?FRAME_PING, Ref, State, Frame) ->
  frame_ping:do_command(Ref, State, Frame);

%%发布内容
do_command(?FRAME_PUB, Ref, State, Frame) ->
  frame_pub:do_command(Ref, State, Frame);

%%发布释放 Qos2发布完成
do_command(?FRAME_PUB_REL, Ref, State, Frame) ->
  frame_pub_rel:do_command(Ref, State, Frame);

%%发布确认QOS1
do_command(?FRAME_PUB_ACK, Ref, State, Frame) ->
  frame_pub_ack:do_command(Ref, State, Frame);

%%发布确认QOS2第一步
do_command(?FRAME_PUB_REC, Ref, State, Frame) ->
  frame_pub_rec:do_command(Ref, State, Frame);
%%发布确认QOS2第三步
do_command(?FRAME_PUB_COMP, Ref, State, Frame) ->
  frame_pub_com:do_command(Ref, State, Frame);

%%订阅指令
do_command(?FRAME_SUB, Ref, State, Frame) ->
  frame_sub:do_command(Ref, State, Frame);
%%取消订阅指令
do_command(?FRAME_UNSUB, Ref, State, Frame) ->
  frame_unsub:do_command(Ref, State, Frame);

do_command(?FRAME_DISCONN, Ref, State, Frame) ->
  frame_disconn:do_command(Ref, State, Frame).