%%%-------------------------------------------------------------------
%%% @author wukai
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 07. 十一月 2017 21:11
%%%-------------------------------------------------------------------
-module(frame_pub_com).
-author("wukai").
-include("../include/mqtt_protocol.hrl").

%% API 保证交付第三步
-export([do_command/3]).
do_command(_, State, Frame) ->
  <<Mid:16>> = Frame#mqtt_frame.left_bytes,
  lager:log(info, self(), "publish complete msgId:~p", [Mid]),
  State.