%%%-------------------------------------------------------------------
%%% @author wukai
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 24. 十一月 2017 17:30
%%%-------------------------------------------------------------------
-module(demo).
-author("wukai").

%% API
-export([demo_get/0,demo_put/0,demo_if/1]).
demo_put() ->

  put(key1, 123),
  put(key2, 456),
  put(key3, 789).

demo_get() ->
get().

demo_if(N)->
  if N>10,N<20 ->true ;true ->false end.