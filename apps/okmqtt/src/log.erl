%%%-------------------------------------------------------------------
%%% @author wukai
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 15. 十一月 2017 16:25
%%%-------------------------------------------------------------------
-module(log).
-author("wukai").

%% API
-export([log/1,log/2]).
log(MSG,List)->
  lager:log(info,self,MSG,List).
log(MSG)->
  lager:log(info,self,MSG).
