%%%-------------------------------------------------------------------
%%% @author wukai
%%% @copyright (C) 2016, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 22. 七月 2016 10:29
%%%-------------------------------------------------------------------
-module(db_util).
-author("wukai").

%% API
-export([do_query/1,save_batch/1]).

%%查询
do_query(Where) -> mnesia:transaction(fun() -> qlc:e(Where)end).
save_batch(Items)->
  F = lists:foreach(fun(Item) -> mnesia:write(Item) end, Items),
  case mnesia:transaction(F) of
    {atomic, Val} ->
      io:format("write offlinemsg sucess ~n"),
      {Val};
    {aborted, Reason} ->
      io:format("write error ~p", [Reason]),
      {error}
  end.