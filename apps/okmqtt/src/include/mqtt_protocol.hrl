%%%-------------------------------------------------------------------
%%% @author wukai
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 25. 十月 2017 14:38
%%%-------------------------------------------------------------------
-author("wukai").
-ifndef(protocol_record).
-define(protocol_record,true ).


-define(UINT, 32 / unsigned - big - integer).
-define(INT, 32 / signed - big - integer).
-define(USHORT, 16 / unsigned - big - integer).
-define(SHORT, 16 / signed - big - integer).
-define(UBYTE, 8 / unsigned - big - integer).
-define(BYTE, 8 / signed - big - integer).


-define(FRAME_CONN,1).
-define(FRAME_CONN_ACK,2).
-define(FRAME_PUB,3).
-define(FRAME_PUB_ACK,4).
-define(FRAME_PUB_REC,5).
-define(FRAME_PUB_REL,6).
-define(FRAME_PUB_COMP,7).
-define(FRAME_SUB,8).
-define(FRAME_SUB_ACK,9).
-define(FRAME_UNSUB,10).
-define(FRAME_UNSUB_ACK,11).
-define(FRAME_PING,12).
-define(FRAME_PING_ACK,13).
-define(FRAME_DISCONN,14).

-record(mqtt_frame,{
  frame_type,%%帧类型
  frame_flag,%%帧标志
  left_bytes::<<>> %%有效载荷和可变报头的字节数组
}).

-record(state,{
  buf = <<>>,%%缓冲区
  client,
  wait_seg,%%等待的数据段
  socket,
  trans,
  clean_session,
  will_falg,%%遗嘱标志
  will_publish,
  next_msgid = 0,
  packetids::list(),
  mqtt_version,
  connected = false::boolean(),
  wait_seq_len,
  keep_alive=60000::integer(),
  link_type=tcp,
  byteslen=[],%%剩余长度解析
  current_frame=#mqtt_frame{}
}).
-endif.


