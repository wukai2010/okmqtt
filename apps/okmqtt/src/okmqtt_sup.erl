%%%-------------------------------------------------------------------
%% @doc okmqtt top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(okmqtt_sup).
-behaviour(supervisor).
-export([start_link/0]).
-export([init/1]).
-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

init([]) ->

    %%重启策略
    RestartStrategy = one_for_one,
    %%重启次数
    MaxRestarts = 1000,
    %%重启间隔时间
    MaxSecondsBetweenRestarts = 60,

    SupFlags = {RestartStrategy, MaxRestarts, MaxSecondsBetweenRestarts},
    %%始终重启
    Restart = permanent,
    %%无条件立即终止
    Shutdown = brutal_kill,
    %%子进程类型
    ChildType = worker,

    Dispatcher = {
        dispatch_server, %%id
        {dispatch_server, start_link, []}, %%MFA
        Restart, Shutdown, ChildType,
        [dispatch_server]
    },
    Cache_server = {
        cache_server, %%id
        {cache_server, start_link, []}, %%MFA
        Restart, Shutdown, ChildType,
        [cache_server]

    },
    Session = {
        session_server, %%id
        {session_server, start_link, []}, %%MFA
        Restart, Shutdown, ChildType,
        [session_server]
    },
    Retain_server = {
        retain_server, %%id
        {retain_server, start_link, []}, %%MFA
        Restart, Shutdown, ChildType,
        [retain_server]
    },
    Sub_Server = {
        subscribe_server, %%id
        {subscribe_server, start_link, []}, %%MFA
        Restart, Shutdown, ChildType, %%spec
        [subscribe_server]
    },

    CDB_Plugin = {
        device_hub, %%id
        {device_hub, start_link, []}, %%MFA
        Restart, Shutdown, ChildType, %%spec
        [device_hub]
    },

    {ok, { SupFlags, [Retain_server,Cache_server,Session,Sub_Server,Dispatcher,CDB_Plugin]} }.


