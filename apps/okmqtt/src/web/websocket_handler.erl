%%%-------------------------------------------------------------------
%%% @author wukai
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 23. 十二月 2017 20:49
%%%-------------------------------------------------------------------
-module(websocket_handler).
-author("wukai").

-export([init/2]).
-export([websocket_init/1]).
-export([websocket_handle/2]).
-export([websocket_info/2]).

init(Req, Opts) ->
  {cowboy_websocket, Req, Opts}.

websocket_init(State) ->
  {ok, State}.

websocket_handle({text, Msg}, State) ->
  [{<<"order_sn">>,_}, {<<"device_sn">>,ClientId}, {<<"port">>,_},{<<"user">>,Username}]= jsx:decode(Msg),
  Topic = <<"cbd/sys/publish/order/start">>,
  TLen = byte_size(Topic),
  P=publish_frame:wrap({Username,ClientId},Topic,TLen,Msg,0,0,0),
  session_server:publish({Username,ClientId},P),
  {ok, State};

websocket_handle(_Data, State) ->
  {ok, State}.

websocket_info({timeout, _Ref, Msg}, State) ->
  {ok, State};
websocket_info(_Info, State) ->
  {ok, State}.